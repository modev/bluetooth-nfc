// Open the Modal

// open modal caballo
function openModalCaballoPiernaTrasera() {
    document.getElementById("modalCaballoPiernaTrasera").style.display = "block";
}
function openModalCaballoPiernaDelantera() {
  document.getElementById("modalCaballoPiernaDelantera").style.display = "block";
}
function openModalCaballoCabeza() {
    document.getElementById("modalCaballoCabeza").style.display = "block";
}

// open modal perro
function openModalPerroPiernaTrasera() {
  document.getElementById("modalPerroPiernaTrasera").style.display = "block";
}

function openModalPerroTronco() {
  document.getElementById("modalPerroTronco").style.display = "block";
}

function openModalPerroCabeza() {
  document.getElementById("modalPerroCabeza").style.display = "block";
}
  
  // Close the Modal

// close modal caballo
function closeModalCaballoPiernaTrasera() {
  document.getElementById("modalCaballoPiernaTrasera").style.display = "none";
}
function closeModalCaballoPiernaDelantera() {
  document.getElementById("modalCaballoPiernaDelantera").style.display = "none";
}
function closeModalCaballoCabeza() {
  document.getElementById("modalCaballoCabeza").style.display = "none";
}

// close modal perro
function closeModalPerroPiernaTrasera() {
  document.getElementById("modalPerroPiernaTrasera").style.display = "none";
}
function closeModalPerroTronco() {
  document.getElementById("modalPerroTronco").style.display = "none";
}
function closeModalPerroCabeza() {
  document.getElementById("modalPerroCabeza").style.display = "none";
}
  
// Slide caballo/ pierna trasera

var slideIndexCPT = 1;
showSlidesCPT(slideIndexCPT);

// Next/previous controls
function plusSlidesCPT(n) {
  showSlidesCPT(slideIndexCPT += n);
}

// Thumbnail image controls
function currentSlideCPT(n) {
  showSlidesCPT(slideIndexCPT = n);
}

function showSlidesCPT(n) {
  var i;
  var slidesCPT = document.getElementsByClassName("slidesCaballoPiernaTrasera");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slidesCPT.length) {slideIndexCPT = 1}
  if (n < 1) {slideIndexCPT = slidesCPT.length}
  for (i = 0; i < slidesCPT.length; i++) {
    slidesCPT[i].style.display = "none";
  }
  for (i = 1; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slidesCPT[slideIndexCPT-1].style.display = "block";
  dots[slideIndexCPT-1].className += " active";
  captionText.innerHTML = dots[slideIndexCPT-1].alt;
}


// Slide caballo/ pierna delantera

var slideIndexCPD = 1;
showSlidesCPD(slideIndexCPD);

// Next/previous controls
function plusSlidesCPD(n) {
  showSlidesCPD(slideIndexCPD += n);
}

// Thumbnail image controls
function currentSlideCPD(n) {
  showSlidesCPD(slideIndexCPD = n);
}

function showSlidesCPD(n) {
  var i;
  var slidesCPD = document.getElementsByClassName("slidesCaballoPiernaDelantera");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slidesCPD.length) {slideIndexCPD = 1}
  if (n < 1) {slideIndexCPD = slidesCPD.length}
  for (i = 0; i < slidesCPD.length; i++) {
    slidesCPD[i].style.display = "none";
  }
  for (i = 1; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slidesCPD[slideIndexCPD-1].style.display = "block";
  dots[slideIndexCPD-1].className += " active";
  captionText.innerHTML = dots[slideIndexCPD-1].alt;
}


// slide caballo/cabeza

// var slideIndexCC = 1;
// showSlidesCC(slideIndexCC);

// Next/previous controls
// function plusSlidesCC(n) {
//   showSlidesCC(slideIndexCC += n);
// }

// Thumbnail image controls
// function currentSlideCC(n) {
//   showSlidesCC(slideIndexCC = n);
// }

// function showSlidesCC(n) {
//   var i;
//   var slidesCC = document.getElementsByClassName("slidesCaballoCabeza");
//   var dots = document.getElementsByClassName("demo");
//   var captionText = document.getElementById("caption");
//   if (n > slidesCC.length) {slideIndexCC = 1}
//   if (n < 1) {slideIndexCC = slidesCC.length}
//   for (i = 0; i < slidesCC.length; i++) {
//     slidesCC[i].style.display = "none";
//   }
//   for (i = 1; i < dots.length; i++) {
//     dots[i].className = dots[i].className.replace(" active", "");
//   }
//   slidesCC[slideIndexCC-1].style.display = "block";
//   dots[slideIndexCC-1].className += " active";
//   captionText.innerHTML = dots[slideIndexCC-1].alt;
// }


// Slide perro/ pierna trasera

var slideIndexPPT = 1;
showSlidesPPT(slideIndexPPT);

// Next/previous controls
function plusSlidesPPT(n) {
  showSlidesPPT(slideIndexPPT += n);
}

// Thumbnail image controls
function currentSlidePPT(n) {
  showSlidesPPT(slideIndexPPT = n);
}

function showSlidesPPT(n) {
  var i;
  var slidesPPT = document.getElementsByClassName("slidesPerroPiernaTrasera");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slidesPPT.length) {slideIndexPPT = 1}
  if (n < 1) {slideIndexPPT = slidesPPT.length}
  for (i = 0; i < slidesPPT.length; i++) {
    slidesPPT[i].style.display = "none";
  }
  for (i = 1; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slidesPPT[slideIndexPPT-1].style.display = "block";
  dots[slideIndexPPT-1].className += " active";
  captionText.innerHTML = dots[slideIndexPPT-1].alt;
}


// Slide perro/ tronco

var slideIndexPT = 1;
showSlidesPT(slideIndexPT);

// Next/previous controls
function plusSlidesPT(n) {
  showSlidesPT(slideIndexPT += n);
}

// Thumbnail image controls
function currentSlidePT(n) {
  showSlidesPT(slideIndexPT = n);
}

function showSlidesPT(n) {
  var i;
  var slidesPT = document.getElementsByClassName("slidesPerroTronco");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slidesPT.length) {slideIndexPT = 1}
  if (n < 1) {slideIndexPT = slidesPT.length}
  for (i = 0; i < slidesPT.length; i++) {
    slidesPT[i].style.display = "none";
  }
  for (i = 1; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slidesPT[slideIndexPT-1].style.display = "block";
  dots[slideIndexPT-1].className += " active";
  captionText.innerHTML = dots[slideIndexPT-1].alt;
}


// slide perro/cabeza

var slideIndexPC = 1;
showSlidesPC(slideIndexPC);

// Next/previous controls
function plusSlidesPC(n) {
  showSlidesPC(slideIndexPC += n);
}

// Thumbnail image controls
function currentSlidePC(n) {
  showSlidesPC(slideIndexPC = n);
}

function showSlidesPC(n) {
  var i;
  var slidesPC = document.getElementsByClassName("slidesPerroCabeza");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slidesPC.length) {slideIndexPC = 1}
  if (n < 1) {slideIndexPC = slidesPC.length}
  for (i = 0; i < slidesPC.length; i++) {
    slidesPC[i].style.display = "none";
  }
  for (i = 1; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slidesPC[slideIndexPC-1].style.display = "block";
  dots[slideIndexPC-1].className += " active";
  captionText.innerHTML = dots[slideIndexPC-1].alt;
}